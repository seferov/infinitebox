<?php

namespace BattlehackBox\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class MainController extends Controller
{
    public function homepageAction()
    {
        if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $response = new RedirectResponse($this->generateUrl('battlehack_box_dashboard_index'));
            return $response;
        }
        return $this->render('BattlehackBoxMainBundle::homepage.html.twig');
    }
}
