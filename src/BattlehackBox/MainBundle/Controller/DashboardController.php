<?php

namespace BattlehackBox\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DashboardController extends Controller
{
    public function indexAction()
    {
        return $this->render('BattlehackBoxMainBundle:Dashboard:index.html.twig');
    }

    public function appsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $apps = $em->getRepository('BattlehackBoxAppBundle:App')->findAll();

        return $this->render('BattlehackBoxMainBundle:Dashboard:apps.html.twig', array(
            'apps' => $apps,
        ));
    }

    public function cardAction(Request $request)
    {
        $card = new \BattlehackBox\CardBundle\Entity\Card();
        $card->setCreatedAt(new \DateTime('now'));

        $form = $this->createFormBuilder($card)
            ->add('name', 'text')
            ->add('email', 'email')
            ->add('Add', 'submit')
            ->getForm();

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($card);
            $em->flush();
            $em->clear();
        }

        return $this->render('BattlehackBoxMainBundle:Dashboard:card.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function usersAction()
    {
        $em = $this->getDoctrine()->getManager();
        $cards = $em->getRepository('BattlehackBoxCardBundle:Card')->findBy(array('user' => $this->getUser()));

        return $this->render('BattlehackBoxMainBundle:Dashboard:users.html.twig', array(
            'cards' => $cards
        ));
    }
}
