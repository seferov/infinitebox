<?php

namespace BattlehackBox\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BattlehackBoxUserBundle:Default:index.html.twig');
    }
}
