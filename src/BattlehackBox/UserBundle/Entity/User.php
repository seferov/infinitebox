<?php

namespace BattlehackBox\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
/**
 * User
 */
class User extends BaseUser
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var \DateTime
     */
    protected $createdAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    /**
     * @var string
     */
    private $uniqid;


    /**
     * Set uniqid
     *
     * @param string $uniqid
     * @return User
     */
    public function setUniqid($uniqid)
    {
        $this->uniqid = $uniqid;

        return $this;
    }

    /**
     * Get uniqid
     *
     * @return string 
     */
    public function getUniqid()
    {
        return $this->uniqid;
    }
    /**
     * @var \BattlehackBox\AppBundle\Entity\App
     */
    private $app;


    /**
     * Set app
     *
     * @param \BattlehackBox\AppBundle\Entity\App $app
     * @return User
     */
    public function setApp(\BattlehackBox\AppBundle\Entity\App $app = null)
    {
        $this->app = $app;

        return $this;
    }

    /**
     * Get app
     *
     * @return \BattlehackBox\AppBundle\Entity\App 
     */
    public function getApp()
    {
        return $this->app;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cards;


    /**
     * Add cards
     *
     * @param \BattlehackBox\CardBundle\Entity\Card $cards
     * @return User
     */
    public function addCard(\BattlehackBox\CardBundle\Entity\Card $cards)
    {
        $this->cards[] = $cards;

        return $this;
    }

    /**
     * Remove cards
     *
     * @param \BattlehackBox\CardBundle\Entity\Card $cards
     */
    public function removeCard(\BattlehackBox\CardBundle\Entity\Card $cards)
    {
        $this->cards->removeElement($cards);
    }

    /**
     * Get cards
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCards()
    {
        return $this->cards;
    }

    /**
     * Set appCoworkerStartedAt
     *
     * @param \DateTime $appCoworkerStartedAt
     * @return User
     */
    public function setAppCoworkerStartedAt($appCoworkerStartedAt)
    {
        $this->appCoworkerStartedAt = $appCoworkerStartedAt;

        return $this;
    }

    /**
     * Get appCoworkerStartedAt
     *
     * @return \DateTime 
     */
    public function getAppCoworkerStartedAt()
    {
        return $this->appCoworkerStartedAt;
    }
    /**
     * @var integer
     */
    private $appFirstAttendeeCount;

    /**
     * @var \DateTime
     */
    private $appCoworkerStartedAt;


    /**
     * Set appFirstAttendeeCount
     *
     * @param integer $appFirstAttendeeCount
     * @return User
     */
    public function setAppFirstAttendeeCount($appFirstAttendeeCount)
    {
        $this->appFirstAttendeeCount = $appFirstAttendeeCount;

        return $this;
    }

    /**
     * Get appFirstAttendeeCount
     *
     * @return integer 
     */
    public function getAppFirstAttendeeCount()
    {
        return $this->appFirstAttendeeCount;
    }
    /**
     * @var integer
     */
    private $appFirstAttendeeLimit;


    /**
     * Set appFirstAttendeeLimit
     *
     * @param integer $appFirstAttendeeLimit
     * @return User
     */
    public function setAppFirstAttendeeLimit($appFirstAttendeeLimit)
    {
        $this->appFirstAttendeeLimit = $appFirstAttendeeLimit;

        return $this;
    }

    /**
     * Get appFirstAttendeeLimit
     *
     * @return integer 
     */
    public function getAppFirstAttendeeLimit()
    {
        return $this->appFirstAttendeeLimit;
    }
}
