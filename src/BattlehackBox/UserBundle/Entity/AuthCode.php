<?php

namespace BattlehackBox\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AuthCode
 */
class AuthCode
{
    /**
     * @var integer
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
