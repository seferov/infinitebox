<?php

namespace BattlehackBox\CardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiController extends Controller
{
    public function postAction()
    {
        $view = new JsonResponse();

        $uniqueId = $this->getRequest()->get('id');//"045912DAA22C80";
        $em = $this->getDoctrine()->getManager();
        $card = $em->getRepository('BattlehackBoxCardBundle:Card')->findOneBy(array('uniqid' => $uniqueId));

        if (!$card) {
            $this->pushWebsocket($uniqueId);
            return $view->setStatusCode(404);
        }

        $user = $card->getUser();
        if (!$user) {
            return $view->setStatusCode(503);
        }

        $app = $user->getApp();
        $response = array();

        switch ($app->getName()) {
            case 'First Attendee':
                $firstAttendeeCount = $user->getAppFirstAttendeeCount();

                if ($firstAttendeeCount <= $user->getAppFirstAttendeeLimit()) {
                    $firstAttendeeCount++;
                    $user->setAppFirstAttendeeCount($firstAttendeeCount);
                    $em->persist($user);
                    $em->flush();
                    $em->clear();

                    $response['status'] = true;
                    $response['message'] = 'Congarutulations! You won a Leap Motion';

                }
                else {
                    $response['status'] = false;
                    $response['message'] = 'Sorry dude! It is too late';
                }

                $this->sendEmail($card->getEmail(), $response['message'], $response['message']);
                break;
            case 'Co-working Space':
                $isLastRead = $card->getLastReadAt() != null && $card->getLastReadAt() != new \DateTime('0000-00-00 00:00:00');
                $isFirstRead = $card->getFirstReadAt() != null && $card->getFirstReadAt() != new \DateTime('0000-00-00 00:00:00');
                if( ($isFirstRead && $isLastRead) || (!$isFirstRead && !$isLastRead) ) {

                    $card->setFirstReadAt(new \DateTime('now'));
                    $card->setLastReadAt(null);
                    $response['message'] = 'Welcome to our co-working space';
                }
                else {
                    $now = new \DateTime('now');
                    $diff = $now->diff($card->getFirstReadAt());
                    $duration = (int) $diff->format('%s');
                    $response['duration'] = $duration;
                    $response['price'] = ($duration * 3 )/ 100;
                    $response['message'] = 'You have been here for '.$duration.' seconds. Price '.$response['price'];

                    $card->setLastReadAt($now);
                }
                $em->persist($card);
                $em->flush();
                $em->clear();

                break;
            case 'Draw':
                $response['message'] = 'To Be Implemented';
                break;
            case 'Bus Ticket':
                $response['message'] = 'To Be Implemented';
                break;
            case 'Game Center':
                $response['message'] = 'To Be Implemented';
                break;
            case 'Custom URL':
                //TODO: output the data from the custom callback url
                break;
        }

        if(isset($response)) {
            return $view->setData($response);
        }

        $view->setData(array(
            'app' => array(
                'name' => $app->getName()
            )
        ));

        return $view;
    }

    private function pushWebsocket($uniqid)
    {
        $url = 'http://api.easysocket.io';
        $privateKey = '24a3a301-ac31-4025-ad4f-fc77492768a5';

        $fields = array(
            'id' => $uniqid,
            'privateKey' => $privateKey
        );

        //url-ify the data for the POST
        $fields_string = '';
        foreach ($fields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');
        //open connection
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        //execute post
        $result = curl_exec($ch);
        //close connection
        curl_close($ch);
    }

    public function infoAction($id)
    {
        $view = new JsonResponse();

        $em = $this->getDoctrine()->getManager();
        $card = $em->getRepository('BattlehackBoxCardBundle:Card')->findOneBy(array('uniqid' => $id));

        if (!$card) {
            return $view->setStatusCode(404);
        }

        $user = $card->getUser();
        if (!$user) {
            return $view->setStatusCode(503);
        }

        $app = $user->getApp();
        $response = array();

        switch ($app->getName()) {
            case 'Co-working Space':
                $isLastRead = $card->getLastReadAt() != null && $card->getLastReadAt() != new \DateTime('0000-00-00 00:00:00');
                $isFirstRead = $card->getFirstReadAt() != null && $card->getFirstReadAt() != new \DateTime('0000-00-00 00:00:00');
                if( ($isFirstRead && $isLastRead) ) {

                    $lastReadAt = $card->getLastReadAt();
                    $diff = $lastReadAt->diff($card->getFirstReadAt());
                    $duration = (int) $diff->format('%s');
                    $response['duration'] = $duration;
                    $response['price'] = $duration * 3;
                    $response['message'] = 'You have been here for '.$duration.' hours';
                }

                break;
            default:
                break;
        }

        if(isset($response)) {
            return $view->setData($response);
        }

        $view->setData(array(
            'app' => array(
                'name' => $app->getName()
            )
        ));

        return $view;
    }

    private function sendEmail($emailAddress, $subject, $body)
    {
        $sendgrid = new \SendGrid('safarov', 'Qusman123456');
        $email = new \SendGrid\Email();
        $email->addTo($emailAddress)
                ->setFrom('info@battlehackbox.com')
                ->setSubject($subject)
                ->setHtml($body);

        $sendgrid->send($email);
    }

    public function addAction(Request $request)
    {
        $response = new JsonResponse();
        $email = $request->get('email');
        $uniqid = $request->get('uniqid');
        $name = $request->get('name');

        if (!$name || !$uniqid || !$email) {
            return $response->setStatusCode(404);
        }

        $card = new \BattlehackBox\CardBundle\Entity\Card();
        $card->setCreatedAt(new \DateTime('now'));
        $card->setEmail($email);
        $card->setUniqid($uniqid);
        $card->setName($name);

        $em = $this->getDoctrine()->getManager();
        $em->persist($card);
        $em->flush();
        $em->clear();

        return $response;
    }
}
