<?php

namespace BattlehackBox\CardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Card
 */
class Card
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $uniqid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Card
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Card
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set uniqid
     *
     * @param string $uniqid
     * @return Card
     */
    public function setUniqid($uniqid)
    {
        $this->uniqid = $uniqid;

        return $this;
    }

    /**
     * Get uniqid
     *
     * @return string 
     */
    public function getUniqid()
    {
        return $this->uniqid;
    }
    /**
     * @var \DateTime
     */
    private $created_at;


    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Card
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }
    /**
     * @var \BattlehackBox\UserBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \BattlehackBox\UserBundle\Entity\User $user
     * @return Card
     */
    public function setUser(\BattlehackBox\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \BattlehackBox\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var \DateTime
     */
    private $lastReadAt;


    /**
     * Set lastReadAt
     *
     * @param \DateTime $lastReadAt
     * @return Card
     */
    public function setLastReadAt($lastReadAt)
    {
        $this->lastReadAt = $lastReadAt;

        return $this;
    }

    /**
     * Get lastReadAt
     *
     * @return \DateTime 
     */
    public function getLastReadAt()
    {
        return $this->lastReadAt;
    }
    /**
     * @var \DateTime
     */
    private $firstReadAt;


    /**
     * Set firstReadAt
     *
     * @param \DateTime $firstReadAt
     * @return Card
     */
    public function setFirstReadAt($firstReadAt)
    {
        $this->firstReadAt = $firstReadAt;

        return $this;
    }

    /**
     * Get firstReadAt
     *
     * @return \DateTime 
     */
    public function getFirstReadAt()
    {
        return $this->firstReadAt;
    }
}
